<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CountriesController extends Controller
{
    // Get Details
    public function getCountryDetails(Request $request)
    {
        //API
        $response = Http::get('https://restcountries.com/v3.1/name/' . $request->name);
        $data = [];

        array_push($data, array(
            'initial' => $response[0]['cca3'],
            'name' => $response[0]['name']['common'],
            'capital' => $response[0]['capital'][0]
        ));
        return json_encode($data);
    }

    // Get Full Details
    public function getCountryFullDetails(Request $request)
    {
         //API
        $response = Http::get('https://restcountries.com/v3.1/name/' . $request->name);
        $data = [];

        array_push($data, array(
            'initial' => $response[0]['cca3'],
            'name' => $response[0]['name']['common'],
            'capital' => $response[0]['capital'][0],
            'region' => $response[0]['region'],
            'flag' => $response[0]['flag'],
            'currency' => $response[0]['currencies'],
            'maps' => $response[0]['maps'],
        ));
        return json_encode($data);
    }
}
